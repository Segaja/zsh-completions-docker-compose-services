function _docker-compose() {
    _arguments \
        '-f[input file]:filename:_files' \
        '*:service:("${(f)$(yq --yaml-output ".services | keys" ${(v)opt_args[(i)-(f|-file)]:-./docker-compose.yml} 2>/dev/null | cut -d\  -f2)}")'
}

compdef _docker-compose docker-compose

function _docker_compose() {
    _arguments \
        "1: :_docker" \
        "*::args:->args"
    case $state in
        args)
            _docker-compose
            ;;
    esac
}

compdef _docker_compose docker
