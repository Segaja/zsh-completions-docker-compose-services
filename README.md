# zsh-completions-docker-compose-services

Provides completions for `docker compose` and `docker-compose` services.

Also can work with alternative docker-compose configuration files given via `-f` and `--files`.


## Usage

Source the [`./docker-compose-service.zsh`](./docker-compose-service.zsh) completion file.


## Dependencies

- https://github.com/kislyuk/yq : needed to parse the `docker-compose` configuration file
  - and it's dependency https://jqlang.github.io/jq/
- the original docker zsh completion files
